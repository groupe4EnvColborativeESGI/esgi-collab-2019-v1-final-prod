<?php

namespace App\Controller;

use App\Service\MixerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\CalculService;

class ServiceController extends AbstractController
{
    /**
     * @Route("/service", name="service")
     */
    public function index(CalculService $calculService)
    {
        return $this->render('home/service.html.twig');
    }
    /**
     * @Route("/service/addition", name="addition")
     */
   public function addition(CalculService $calculservice)
    {
        $a = rand();
        $b= rand();
$a = $_POST['a'];
        $b = $_POST['b'];
        $calculservice->Addition($a,$b);
        return $this->render('home/service.html.twig');
    }
   /**
     * @Route("/service/soustraction", name="soustraction")
     */
    public function soustraction(CalculService $calculservice)
    {
        $a = rand();
        $b= rand();
        $calculservice->Substraction($a,$b);
        return $this->render('home/service.html.twig');
    }
    /**
     * @Route("/service/multiplaction", name="multiplaction")
     */
    public function multiplication(CalculService $calculservice)
    {
        $a = rand();
        $b= rand();
        $calculservice->Multiplication($a,$b);
        return $this->render('home/service.html.twig');
    }
    /**
     * @Route("/service/division", name="division")
     */
    public function division(CalculService $calculservice)
    {
        $a = rand();
        $b= rand();
        $calculservice->Division($a,$b);
        return $this->render('home/service.html.twig');
    }
}
