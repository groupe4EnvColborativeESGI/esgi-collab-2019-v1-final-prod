<?php

namespace App\Service;


class CalculService{
    public function Addition(float $a, float $b): float
    {
        return $a+$b;
    }

    public function Substraction(float $a, float $b): float
    {
        return $a-$b;
    }

    public function Multiplication(float $a, float $b): float
    {
        return $a*$b;
    }

    public function Division(float $a, float $b): float
    {
        return $a/$b;
    }
}